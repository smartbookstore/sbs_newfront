import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LivreService } from '../service/livre.service';

@Component({
  selector: 'app-delete',
  templateUrl: './delete.component.html',
  styleUrls: ['./delete.component.css']
})
export class DeleteComponent implements OnInit {

  cin: number; 
  constructor(public uss:LivreService,public ar:ActivatedRoute, private router : Router) {
    this.ar.params.subscribe(
      data => {this.cin = data.cin; }
    
    );
   }
   validerr(){
    this.uss.supprimerBook(this.cin).subscribe(
      error=>{alert("Error");},
      data=>{alert("Book Deleted");this.cin=0;
      this.router.navigate(['/admin1']);}
    
     
    )
    }
   annuler()
{
  alert('Failed Delete Operation');
}

  ngOnInit(): void {
  }

}

import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../service/user.service';

@Component({
  selector: 'app-login1',
  templateUrl: './login1.component.html',
  styleUrls: ['./login1.component.css']
})
export class Login1Component implements OnInit {

  loginForm: FormGroup ;
  constructor(private pnlog: UserService , private route: ActivatedRoute, fb:FormBuilder, private router: Router) {
      this.loginForm =   fb.group({
        adresse: ['', Validators.required],
        motPasse: ['', Validators.required],
      });
     }
 
get f (){
  return this.loginForm.controls ; 
}
  


  // tslint:disable-next-line:typedef
  login(){
    let x={email: this.f.adresse.value ,motPasse:this.f.motPasse.value }
 this.pnlog.loginuser(x).subscribe(
  (user)=>{
    //localStorage.setItem('currentUser',JSON.stringify(user));
    this.router.navigate(['/admin']);
    console.log("Succes")
  } ,
  (error)=>{
    alert("Verify Your Coordinates") ; 
  }
 )
  
 }


  ngOnInit(): void {
  }

}

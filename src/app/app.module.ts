import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { InscriptionComponent } from './inscription/inscription.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { UserComponent } from './user/user.component';
import { NavbarComponent } from './navbar/navbar.component';
import { HomeComponent } from './home/home.component';
import { FooterComponent } from './footer/footer.component';
import { LoginComponent } from './login/login.component';
import { ModifierComponent } from './modifier/modifier.component';
import { SupprimerComponent } from './supprimer/supprimer.component';
import { BookComponent } from './book/book.component';
import { AjoutComponent } from './ajout/ajout.component';
import { AdminComponent } from './admin/admin.component';
import { ContactComponent } from './contact/contact.component';
import { ProfilComponent } from './profil/profil.component';

import { LivreComponent } from './livre/livre.component';
import { Admin1Component } from './admin1/admin1.component';
import { NgbdModalConfirmComponent } from './ngbd-modal-confirm/ngbd-modal-confirm.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AboutComponent } from './about/about.component';
import { EditComponent } from './edit/edit.component';
import { DeleteComponent } from './delete/delete.component';
import { Login1Component } from './login1/login1.component';
import { Livre1Component } from './livre1/livre1.component';
import { Livre2Component } from './livre2/livre2.component';

@NgModule({
  declarations: [
    AppComponent,
    InscriptionComponent,
    UserComponent,
    NavbarComponent,
    HomeComponent,
    FooterComponent,
    LoginComponent,
    ModifierComponent,
    SupprimerComponent,
    BookComponent,
    AjoutComponent,
    AdminComponent,
    ContactComponent,
    ProfilComponent,

    LivreComponent,
    Admin1Component,
    NgbdModalConfirmComponent,
    AboutComponent,
    EditComponent,
    DeleteComponent,
    Login1Component,
    Livre1Component,
    Livre2Component,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    NgbModule,
 

    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { UserService } from '../service/user.service';
import { User } from '../models/user';



@Component({
  selector: 'app-inscription',
  templateUrl: './inscription.component.html',
  styleUrls: ['./inscription.component.css']
})
export class InscriptionComponent implements OnInit {
  users:any;
  constructor(private http: HttpClient,private us:UserService) { }
  verifier (f:NgForm) 
  { 
   let cin=f.value["cin"];
  let nom = f.value["nom"]; 
  let prenom = f.value["prenom"]; 
  let mail = f.value["adresse"];
  let password = f.value["motPasse"];
  let Role = f.value["Role"];
    let user= new User(cin,nom,prenom,mail,password,Role);
        this.us.ajoutUser(user)
        .subscribe((User)=> {
           this.users = [User, this.users];})
         
  alert("Utilisateur Added Succesfully ");
  
    }
    
  



  ngOnInit(): void {
  }
}
import { HttpClient } from '@angular/common/http';
import { Component, Inject, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from '../models/user';
import { UserService } from '../service/user.service';

@Component({
  selector: 'app-modifier',
  templateUrl: './modifier.component.html',
  styleUrls: ['./modifier.component.css']
})
export class ModifierComponent implements OnInit {
  users: any;
  uu: User;
  constructor(private http: HttpClient, private us: UserService, public ar: ActivatedRoute , private router : Router) {
    let cin: number;
    this.ar.params.subscribe(
      data => { cin = data.cin; }
    );
    this.us.getuser(cin).subscribe(
      data => { this.uu = data; }
    );
  }
  modifier(f: NgForm) {
    let cin = this.uu.cin;
    let nom = f.value["nom"];
    let prenom = f.value["prenom"];
    let mail = f.value["adresse"];
    let password = f.value["motPasse"];
    let role = f.value["role"];
    let user = new User(cin, nom, prenom, mail, password, role);
    let x = { cin: cin, ...user };
    this.us.modifieruserr(x)
      .subscribe(data => {
        this.users = data;
        alert("User Updated Succefully ");
        this.router.navigate(['/admin']);
      },
      error=>{
        console.log(error);
        
      })
    
  }

  ngOnInit(): void {
  }

}



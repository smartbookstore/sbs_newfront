import { ContactComponent } from './contact/contact.component';
import { AdminComponent } from './admin/admin.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BookComponent } from './book/book.component';
import { HomeComponent } from './home/home.component';
import { InscriptionComponent } from './inscription/inscription.component';
import { LoginComponent } from './login/login.component';
import { ModifierComponent } from './modifier/modifier.component';
import { NavbarComponent } from './navbar/navbar.component';
import { SupprimerComponent } from './supprimer/supprimer.component';
import { UserComponent } from './user/user.component';
import { ProfilComponent } from './profil/profil.component';
import { LivreComponent } from './livre/livre.component';

import { Admin1Component } from './admin1/admin1.component';

import { AjoutComponent } from './ajout/ajout.component';
import { AboutComponent } from './about/about.component';
import { EditComponent } from './edit/edit.component';
import { DeleteComponent } from './delete/delete.component';
import { Login1Component } from './login1/login1.component';
import { Livre1Component } from './livre1/livre1.component';
import { Livre2Component } from './livre2/livre2.component';

const routes: Routes = [
  {path:'navbar', component:NavbarComponent},
  {path:'', component:HomeComponent},
  {path:'user', component:UserComponent},
  {path:'inscription',component:InscriptionComponent},
  {path:'book',component:BookComponent},
  {path:'modifier/:cin',component:ModifierComponent},
  {path:'supprimer/:cin',component:SupprimerComponent},
  {path:'login',component:LoginComponent},
  {path:"admin",component:AdminComponent},
  {path:"admin1",component:Admin1Component},
  {path:"contact",component:ContactComponent},
  {path:"profil",component:ProfilComponent},
  {path:"livre",component:LivreComponent},
  {path:"livre1",component:Livre1Component},
  {path:"livre2",component:Livre2Component},
  {path:"Ajout",component:AjoutComponent},
  {path:"about",component:AboutComponent},
  {path:"login1",component:Login1Component},
  {path:'edit/:cin',component:EditComponent},
  {path:'delete/:cin',component:DeleteComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

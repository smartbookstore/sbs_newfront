import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Livre1Component } from './livre1.component';

describe('Livre1Component', () => {
  let component: Livre1Component;
  let fixture: ComponentFixture<Livre1Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Livre1Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Livre1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

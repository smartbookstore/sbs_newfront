import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Livre } from '../models/livre';

@Injectable({
  providedIn: 'root'
})
export class LivreService {

  apiUrl="http://localhost:8484/WebServiceLogin/rest/Book";

  constructor(public http:HttpClient) {}

  ajoutBook(livres:any)
  {
    return this.http.post<Livre>(this.apiUrl,livres);
  }
  getBooks()
  {
    return this.http.get<Livre[]>(this.apiUrl);
  }
  supprimerBook(cin:number)
  {
    return this.http.delete(this.apiUrl+"/"+cin);
  }
  getBook(cin:number)
  {
    return this.http.get<Livre>(this.apiUrl+"/"+cin);
  }
  modifierBook(l:Livre)
  {
    return this.http.put(this.apiUrl,l);
  }  
}

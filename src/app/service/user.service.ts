import { HttpClient} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { User } from '../models/user';


@Injectable({
  providedIn: 'root'
})
export class UserService {
 

  apiUrl="http://localhost:8484/WebServiceLogin/rest/log";

  constructor(public http:HttpClient) {}

  ajoutUser(users:any)
  {
    return this.http.post<User>(this.apiUrl,users);
  }
  getTousUserS()
  {
    return this.http.get<User[]>(this.apiUrl);
  }
  supprimeruserr(id:number)
  {
    return this.http.delete(this.apiUrl+"/"+id);
  }
  getuser(id:number)
  {
    return this.http.get<User>(this.apiUrl+"/"+id);
  }
  modifieruserr(u:User)
  {
    return this.http.put(this.apiUrl,u);
  } 
  loginuser(user:any){
    return this.http.post<any>("http://localhost:8484/WebServiceLogin/rest/log/login",user) ;
  } 

 
}


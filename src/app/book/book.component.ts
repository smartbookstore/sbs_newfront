import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { Livre } from '../models/livre';
import { LivreService } from '../service/livre.service';


@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.css']
})
export class BookComponent implements OnInit {
  url="http://localhost:8484/WebServiceLogin/rest/Book";
  
  livres:any;
  constructor(private http: HttpClient,private ls:LivreService,  private router : Router) { }
  verifierr(f:NgForm) 
  { 
  let cin=f.value["cin"];
  let titre= f.value["titre"]; 
  let auteur= f.value["auteur"]; 
  let reb = f.value["Rubrique"];
  let s = f.value["Stock"];
 
    let book= new Livre(cin,titre,auteur,reb,s);
        this.ls.ajoutBook(book)
        .subscribe((Livre)=> {
           this.livres = [Livre, this.livres];})
         
  alert("Book Added Succefully!");
  this.router.navigate(['/admin1']);
    }
    
  

  ngOnInit(): void {
  }

}

import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';



@Component({
  selector: 'app-admin1',
  templateUrl: './admin1.component.html',
  styleUrls: ['./admin1.component.css']
})
export class Admin1Component implements OnInit {

  uri="http://localhost:8484/WebServiceLogin/rest/Book";
  lesbooks;
  constructor(private http:HttpClient) {
    this.http.get(this.uri).subscribe((data)=>{this.lesbooks=data});
  }

  ngOnInit(): void {
  }

}

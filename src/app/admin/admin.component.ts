import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { User } from '../models/user';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

  uri="http://localhost:8484/WebServiceLogin/rest/log";
  lesusers;
  
  public currentUser: User | undefined;
  public currentUserItem: string | null = localStorage.getItem('currentUser');
  constructor(private http:HttpClient) {
    this.http.get(this.uri).subscribe((data)=>{this.lesusers=data});
   this.currentUser = this.currentUserItem !== null ? JSON.parse(this.currentUserItem) : null;
  }


  ngOnInit(): void {
  }

}

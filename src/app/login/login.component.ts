
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../service/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup ;
  constructor(private pnlog: UserService , private route: ActivatedRoute, fb:FormBuilder, private router: Router) {
      this.loginForm =   fb.group({
        adresse: ['', Validators.required],
        motPasse: ['', Validators.required],
      });
     }
 
get f (){
  return this.loginForm.controls ; 
}
  ngOnInit(): void {

  }


  // tslint:disable-next-line:typedef
  login(){
    let x={email: this.f.adresse.value ,motPasse:this.f.motPasse.value }
 this.pnlog.loginuser(x).subscribe(
  (user)=>{
    //role du user
   //localStorage.setItem('currentUser',JSON.stringify(user));
    this.router.navigate(['/profil']);
    console.log("Succes")
  } ,
  (error)=>{
    alert("Verify Your Coordinates") ; 
  }
 )
  
 }

  }

  
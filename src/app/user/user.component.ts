import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  uri="http://localhost:8484/WebServiceLogin/rest/log";
  lesusers;
  constructor(private http:HttpClient) {
    this.http.get(this.uri).subscribe((data)=>{this.lesusers=data});
  }

  ngOnInit(): void {
    
  }

}




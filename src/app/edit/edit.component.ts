import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Livre } from '../models/livre';
import { LivreService } from '../service/livre.service';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {

  books: any;
  ss: Livre;
  constructor(private http: HttpClient, private uss: LivreService, public ar: ActivatedRoute , private router : Router) {
    let cin: number;
    this.ar.params.subscribe(
      data => { cin = data.cin; }
    );
    this.uss.getBook(cin).subscribe(
      data => { this.ss = data; }
    );
  }
  modifierr(f: NgForm) {
    let cin = this.ss.cin;
    let auteur = f.value["auteur"];
    let rebrique = f.value["rebrique"];
    let titre = f.value["titre"];
    let stock = f.value["stock"];
 
    let book = new Livre(cin, auteur, rebrique, titre, stock);
    let x1 = { cin: cin, ...book };
    this.uss.modifierBook(x1)
      .subscribe(data => {
        this.books = data;
        alert("Book Updated Succefully!");
        this.router.navigate(['/admin1']);
      },
      error=>{
        console.log(error);
        
      })
    
  }

  ngOnInit(): void {
  }

}

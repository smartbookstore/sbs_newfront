import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Livre2Component } from './livre2.component';

describe('Livre2Component', () => {
  let component: Livre2Component;
  let fixture: ComponentFixture<Livre2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Livre2Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Livre2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

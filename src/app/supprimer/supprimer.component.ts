import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { UserService } from '../service/user.service';

@Component({
  selector: 'app-supprimer',
  templateUrl: './supprimer.component.html',
  styleUrls: ['./supprimer.component.css']
})
export class SupprimerComponent implements OnInit {
  cin: number; 
  constructor(public us:UserService,public ar:ActivatedRoute, private router : Router) {
    this.ar.params.subscribe(
      data => {this.cin = data.cin; }
    
    );
   }
   valider(){
    this.us.supprimeruserr(this.cin).subscribe(
      data=>{alert("User Deleted");this.cin=0;
      this.router.navigate(['/admin']);},
    
      error=>{alert("Error");}
    )
    }
   annuler()
{
  alert('Failed Delete Operation');
}
  ngOnInit(): void {
  }

}

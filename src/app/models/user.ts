export class User {

    cin : number;
    nom : string;
    prenom : string;
    adresse : string;
    motPasse : string;
    role : string;

  
    constructor(cin:number, nom:string, prenom:string, adresse:string, motPasse:string,role:string)
    {
      this.cin = cin;
      this.nom = nom;
      this.prenom = prenom;
      this.adresse = adresse;
      this.motPasse = motPasse;
      this.role=role;
    }
  }
  